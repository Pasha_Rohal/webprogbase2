const express=require('express');

const Story = require('../models/story');
const User = require('../models/user');
const routes = express.Router();

routes.get("/:id",checkAuth,function(req,res){
    let admin = true;
    const role = req.user.role;
    if(role === 0){
        admin = false;
    }
    const id = req.params.id;
    Story.getById(id)
    .then(story => {
        if(typeof story === "undefined"){
             res.status(404).send(`User with id ${id} not found`);
        }else{
            res.render("tasks",{story:story, user:req.user,admin:admin});
        }
    })
    .catch(err => res.render(`error`,{error:err.toString(),code:404,user:req.user}));})

routes.get("/",checkAuth ,function (req, res) {
    let last = 0;
            let page = req.query.page;
            let search = "";
            const sort = [];
            const arrUser = [];
            User.getAll()
            .then(users=>{
            if(req.query.search){
                search = req.query.search;
            }
            if(req.query.p){
                search = req.query.p;
            }
            if (page === undefined && search ==="") {
                res.redirect("/tests?page=1")
                return;
            }else if(page === undefined){
                res.redirect(`/tests?page=1&search=${search}`);
                return;
            }
            page = parseInt(page);
            if(search === ""){
                for (let i = (page - 1) * 5; i < page * 5 && i < users.length; i++) {
                    arrUser.push(users[i]);
                    }
                    last = Math.ceil(users.length / 5);
            }else{
                for(let i = 0;i < users.length;i++){     
                const login = users[i].login.toUpperCase();
                    if(login.includes(search.toUpperCase())){
                        sort.push(users[i]);
                    }
                }
                for (let i = (page - 1) * 5; i < page * 5 && i < sort.length; i++) {
                    arrUser.push(sort[i]);
                }
                last =  Math.ceil(sort.length / 5);
            }
        const next = page + 1;
        const prev = page - 1; 
        const First = page != 1;
        const Thelast = next - 1 != last;

        res.render("tests", {users:arrUser, page, Thelast, next, prev,last, First, isNull:last === 0,search, user:req.user})
        
        } )
        .catch(err => res.render(`error`,{error:err.toString(),code:404,user:req.user}));

});

function checkAdmin(req, res, next) {
    if (!req.user) res.sendStatus(401); // 'Not authorized'
    else if (req.user.role !== 'admin') res.sendStatus(403); // 'Forbidden'
    else next();  // пропускати далі тільки аутентифікованих із роллю 'admin'
}

function checkAuth(req, res, next) {
    if (!req.user) return res.redirect(`/auth/registering`);  // 'Not authorized'
    next();  // пропускати далі тільки аутентифікованих
}


module.exports = routes;