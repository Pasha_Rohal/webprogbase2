const express = require('express');
const User = require('../models/user');
const routes = express.Router();
const Story = require('../models/story')
const Actual = require('../models/actual')

const config = require('../config');
const cloudinary = require('cloudinary');

cloudinary.config({
    cloud_name: config.cloudinary.cloud_name,
    api_key: config.cloudinary.api_key,
    api_secret: config.cloudinary.api_secret
});

routes.get("/", checkAuth, function (req, res) {
    User.getAll()
        .then(users => res.render("users", {
            users: users, user: req.user
        }))
        .catch(err => res.render(`error`,{error:err.toString(),code:500,user:req.user})); 
});

routes.get("/:id/update", checkAuth, function (req, res) {

    const id = req.params.id;
    User.getById(id)
        .then(users => {
            res.render('updateForm', { users: users, user: req.user })
        })
        .catch(err => res.render(`error`,{error:err.toString(),code:500,user:req.user}));
    });
routes.get("/:id/addStory", checkAuth, function (req, res) {

    const id = req.params.id;
    User.getById(id)
        .then(users => {
            res.render('addstory', { users: users, user: req.user })
        })
        .catch(err => res.render(`error`,{error:err.toString(),code:500,user:req.user}));});

routes.get("/:id/actual/:idl", checkAuth, function (req, res) {
    let idbool;
    const idl = req.params.idl;
    if (users.id === req.user.id) {
        idbool = true;
    }
    else {
        idbool = false;
    }
    User.getById(id)
        .then(users => {
            res.render('storyid', {idbool:idbool, users: users, user: req.user })
        })
        .catch(err => res.render(`error`,{error:err.toString(),code:500,user:req.user}));})

        function timeStatus(story) {
            let today = new Date();
            let newArray = [];
            for (let i = 0; i < story.length; i++) {
                {
                    if ((today - story[i].registeredStory) / 300000 < 1) {
                        newArray.push(story[i]);
                    }
                }
            }
            return newArray;
        }
routes.get("/:id", checkAuth, function (req, res) {
    let idbool;
    let admin;
    const id = req.params.id;
    User.getById(id)
        .then(users => {
            if (users.id === req.user.id) {
                idbool = true;
            }
            else {
                idbool = false;
            }
            if(req.user.role === 1){
                admin = true;
            }else{
                admin = false;
            }
            if (typeof users === "undefined") {
                res.status(404).send(`User with id ${id} not found`);
            } else 
            {
                Story.getAll()
                .then(storys => {
                    let story = timeStatus(storys)
                    const arrayUser = [];
                    console.log(users.actual.length);
                    console.log(story.length);
                    for (let i = 0; i < users.actual.length; i++) {
                        for (let k = 0; k < story.length; k++) {
                            if (users.actual[i] == story[k].id) {
                                arrayUser.push(story[k]);
                            }
                        }
                    }
                   res.render("user", {admin:admin, users: users, user: req.user, idbool: idbool, story: arrayUser });
                    })
                    .catch(err => res.render(`error`,{error:err.toString(),code:500,user:req.user}));
            }
        })
        .catch(err => res.render(`error`,{error:err.toString(),code:500,user:req.user}));
})

function checkAdmin(req, res, next) {
    if (!req.user) res.sendStatus(401); // 'Not authorized'
    else if (req.user.role !== 1) res.sendStatus(403); // 'Forbidden'
    else next();  // пропускати далі тільки аутентифікованих із роллю 'admin'
}

function checkAuth(req, res, next) {
    if (!req.user) return res.redirect(`/auth/registering`);
    next();
}


module.exports = routes;