const fs = require('fs');
const mongoose = require('mongoose');
const Actual = require("./actual");

const userSchema = new mongoose.Schema({
    login:{type:String,require:true},
    password:{type:String},
    registeredAt:{type:Date,default:Date.now},
    avaUrl: {type: String},
    role: {type:Number},
    email:{type:String},
    isDisabled:{type:Boolean},
    actual:{type:Array},
    masfollowers:{type:Array},
    maslikes:{type:Array},
    followers:{type:Number},
    likes:{type:Number},
    biography:{type:String},
    job:{type:String},
});
const UserModel = mongoose.model('User',userSchema);


class User {

    constructor(login,password,registeredAt,avaUrl,role,email,isDisabled,actual,masfollowers,maslikes,followers,likes,biography,job) {
        this.login = login;
        this.password = password;
        this.registeredAt = registeredAt;
        this.avaUrl = avaUrl;
        this.role = role;
        this.email = email;
        this.isDisabled = isDisabled;
        this.actual = [];
        this.masfollowers = [];
        this.maslikes = [];
        this.followers = followers;
        this.likes = likes;
        this.biography = biography;
        this.job = job;
    }

    static getAll() {
        return UserModel.find();
    }

    static getById(id) {
        return UserModel.findById(id);
    }
    static insert(user){
        return  new UserModel(user).save();
    }
    static update(user){
        return UserModel.update(
        {"_id": user.id},
        {"$set":{
        "login" : user.login,
        "password" : user.password,
        "registeredAt" : user.registeredAt,
        "avaUrl" : user.avaUrl,
        "role" : user.role,
        "email": user.email,
        "isDisabled" : user.isDisabled,
        "actual" : user.actual,
        "masfollowers": user.masfollowers,
        "maslikes": user.maslikes,
        "followers": user.followers,
        "likes": user.likes,
        "biography": user.biography,
        "job": user.job
        }});
    }
    static delete(id) {
        return UserModel.remove({"_id":(id)}).exec();
    }
}
module.exports = User;