const fs = require('fs');

const mongoose = require('mongoose');

const storySchema = new mongoose.Schema({
    name:{type:String,require:true},
    genre:{type:String},
    registeredStory:{type:Date,default:Date.now},
});
const storyModel = mongoose.model('Story',storySchema);

class Story {
    constructor( name, genre,registeredStory) {
        this.name = name;
        this.genre = genre;
        this.registeredStory = registeredStory;
    }
    static insert(story) {
        return  new storyModel(story).save();
    }
    static getAll() {
        return storyModel.find().exec();
    }
    static getById(id,) {
        return storyModel.findById(id).exec();
    }
    static delete(id) {
        return storyModel.remove({"_id":(id)}).exec();
    }
}
module.exports = Story;